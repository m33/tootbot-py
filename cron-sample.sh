# activate virtualenv if necessary
# source /home/YOUR_HOME_DIR/.virtualenvs/tootbot/bin/activate

# parameters:
# 1- twitter account to clone / or rss/atom feed URL
# 2- mastodon user account (@your_account)
# 3- mastodon password
# 4- instance domain (https:// is automatically added)
# 5- max age (in days)
# 6- footer tags to add
# 7- delay
# 8- mastodon email account (your_email_login@domain.com)

python3 tootbot.py https://your-rssfeed-source.com/recent.atom @your_mastodon_username_account@botsin.space **password** botsin.space 2 #your_hashtag 0 your_mastondon_email_account
