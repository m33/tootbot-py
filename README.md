# TootBot

A small python 3.x script to replicate tweets on a mastodon account.

Forked from https://github.com/cquest/tootbot
Specialized in RSS feed, in particular from Nitter (https://nitter.net - https://github.com/zedeus/nitter)

It gets the tweets from RSS available at https://nitter.net, then does some cleanup on the content:
- twitter tracking links (t.co) are dereferenced
- twitter hosted pictures are retrieved and uploaded to mastodon
- the tweets from RSS source's are joined based on the domain name to avoid duplicates

It can also toot RSS/atom feeds (see cron-example.sh).

A sqlite database is used to keep track of tweets than have been tooted.

The script is simply called by a cron job and can run on any server.

## Setup

```
# clone this repo
git clone https://git.qoto.org/m33/tootbot-py.git
cd tootbot

# install required python modules
pip3 install -r requirements.txt
```

## Configuration file

The configuration file should be placed in a safe directory (not world or group readable), and contains the instance and authentication credentials. 
You must complete the config file and provide authentication credentials to your desired mastodon account. 
A sample config file 'tootbot@my_bot@my_instance.conf' is provided as a starting point.

## Authentication methods

This app support both email and application token authentication.
You are strongly encouraged to create an application with dedicated app id, secret and access token for this bot.
See the "Development" and "New application" pannel in your mastodon account settings.

## Useage

With a plain RSS/atom feed:

`python3 tootbot.py [full path to authentication config file] https://your-rssfeed-source.com/recent.atom 2 your_hashtag 0`

(all arguments are mandatory)

